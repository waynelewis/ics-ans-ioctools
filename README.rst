ics-ans-ioctools
================

Ansible playbook to install IOC server tools

Variables
---------

Usage
-----

The playbook can be run locally or from an Ansible server.

To run locally on the machine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A script is provided to easily provision a machine.
It will install ansible locally.
This is for testing purpose and not recommended for production.

Download the script `bootstrap-local.sh` and run it as root::

    $ cd /tmp
    $ curl -O https://bitbucket.org/europeanspallationsource/ics-ans-ioctools/raw/master/bootstrap-local.sh
    $ chmod a+x /tmp/bootstrap-local.sh
    $ sudo /tmp/bootstrap-local.sh

This will use the default variables. If you want to make changes, you can use the "--no-run" option
and run manually the ansible-playbook command::

    $ sudo /tmp/bootstrap-local.sh --no-run
    Set variables in /etc/ansible/host_vars/localhost
    $ sudo /usr/local/bin/ansible-playbook /etc/ansible/playbook.yml


To run from an Ansible server
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You must have ansible >= 2.4.0.0 already installed.
You should be able to ssh to the server to install and have sudo rights.

::

    $ git clone https://bitbucket.org/europeanspallationsource/ics-ans-ioctools.git
    $ cd ics-ans-ioctools
    # Edit the "hosts" file
    $ make playbook


Testing
-------

No testing configured at the moment.

License
-------

BSD 2-clause
